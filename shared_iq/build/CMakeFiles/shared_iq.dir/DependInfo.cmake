# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/Desktop/MMP_SOUND/shared_iq/shared_iq.c" "/root/Desktop/MMP_SOUND/shared_iq/build/CMakeFiles/shared_iq.dir/shared_iq.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "LINUX"
  "__LINUX__"
  "__POSIX__"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
