#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#define SHARED_IQ_EXPORTS
#include "shared_iq.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <pulse/pulseaudio.h>

/**
 * Read and parse a wave file
 *
 **/
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wave.h"
#define TRUE 1 
#define FALSE 0

// WAVE header structure

unsigned char buffer4[4];
unsigned char buffer2[2];

char* seconds_to_time(float seconds);


 FILE *ptr;
 char *filename = "/root/Desktop/MMP_SOUND/test.wav";
 struct HEADER header;

int mread = 0;

 float duration_in_seconds;
 long num_samples;
 long size_of_each_sample;

long i_container =0;
int  size_is_correct = TRUE;

long bytes_in_each_channel;

long low_limit = 0l;
long high_limit = 0l;

/**
 * Convert seconds into hh:mm:ss format
 * Params:
 *	seconds - seconds value
 * Returns: hms - formatted string
 **/
 char* seconds_to_time(float raw_seconds) {
  char *hms;
  int hours, hours_residue, minutes, seconds, milliseconds;
  hms = (char*) malloc(100);

  sprintf(hms, "%f", raw_seconds);

  hours = (int) raw_seconds/3600;
  hours_residue = (int) raw_seconds % 3600;
  minutes = hours_residue/60;
  seconds = hours_residue % 60;
  milliseconds = 0;

  // get the decimal part of raw_seconds to get milliseconds
  char *pos;
  pos = strchr(hms, '.');
  int ipos = (int) (pos - hms);
  char decimalpart[15];
  memset(decimalpart, ' ', sizeof(decimalpart));
  strncpy(decimalpart, &hms[ipos+1], 3);
  milliseconds = atoi(decimalpart);	

  
  sprintf(hms, "%d:%d:%d.%d", hours, minutes, seconds, milliseconds);
  return hms;
}



// to enabled the downsampler
// #define DO_DOWNSAMPLE_SHARED_IQ

// Field list is here: http://0pointer.de/lennart/projects/pulseaudio/doxygen/structpa__sink__info.html
typedef struct pa_devicelist {
	uint8_t initialized;
	char name[512];
	uint32_t index;
	char description[256];
} pa_devicelist_t;

void pa_state_cb(pa_context *c, void *userdata);
void pa_sinklist_cb(pa_context *c, const pa_sink_info *l, int eol, void *userdata);
void pa_sourcelist_cb(pa_context *c, const pa_source_info *l, int eol, void *userdata);
int pa_get_devicelist(pa_devicelist_t *input, pa_devicelist_t *output);

//Audio Methods

// This callback gets called when our context changes state.  We really only
// care about when it's ready or if it has failed
void pa_state_cb(pa_context *c, void *userdata) {
	pa_context_state_t state;
	int *pa_ready = userdata;

	state = pa_context_get_state(c);
	switch  (state) {
		// There are just here for reference
		case PA_CONTEXT_UNCONNECTED:
		case PA_CONTEXT_CONNECTING:
		case PA_CONTEXT_AUTHORIZING:
		case PA_CONTEXT_SETTING_NAME:
		default:
			break;
		case PA_CONTEXT_FAILED:
		case PA_CONTEXT_TERMINATED:
			*pa_ready = 2;
			break;
		case PA_CONTEXT_READY:
			*pa_ready = 1;
			break;
	}
}

// pa_mainloop will call this function when it's ready to tell us about a sink.
// Since we're not threading, there's no need for mutexes on the devicelist
// structure
void pa_sinklist_cb(pa_context *c, const pa_sink_info *l, int eol, void *userdata) {
	pa_devicelist_t *pa_devicelist = userdata;
	int ctr = 0;

	// If eol is set to a positive number, you're at the end of the list
	if (eol > 0) {
		return;
	}

	// We know we've allocated 16 slots to hold devices.  Loop through our
	// structure and find the first one that's "uninitialized."  Copy the
	// contents into it and we're done.  If we receive more than 16 devices,
	// they're going to get dropped.  You could make this dynamically allocate
	// space for the device list, but this is a simple example.
	for (ctr = 0; ctr < 16; ctr++) {
		if (! pa_devicelist[ctr].initialized) {
			strncpy(pa_devicelist[ctr].name, l->name, 511);
			strncpy(pa_devicelist[ctr].description, l->description, 255);
			pa_devicelist[ctr].index = l->index;
			pa_devicelist[ctr].initialized = 1;
			break;
		}
	}
}

// See above.  This callback is pretty much identical to the previous
void pa_sourcelist_cb(pa_context *c, const pa_source_info *l, int eol, void *userdata) {
	pa_devicelist_t *pa_devicelist = userdata;
	int ctr = 0;

	if (eol > 0) {
		return;
	}

	for (ctr = 0; ctr < 16; ctr++) {
		if (! pa_devicelist[ctr].initialized) {
			strncpy(pa_devicelist[ctr].name, l->name, 511);
			strncpy(pa_devicelist[ctr].description, l->description, 255);
			pa_devicelist[ctr].index = l->index;
			pa_devicelist[ctr].initialized = 1;
			break;
		}
	}
}

int pa_get_devicelist(pa_devicelist_t *input, pa_devicelist_t *output) {
	// Define our pulse audio loop and connection variables
	pa_mainloop *pa_ml;
	pa_mainloop_api *pa_mlapi;
	pa_operation *pa_op = 0;
	pa_context *pa_ctx;


	// We'll need these state variables to keep track of our requests
	int state = 0;
	int pa_ready = 0;

	// Initialize our device lists
	memset(input, 0, sizeof(pa_devicelist_t) * 16);
	memset(output, 0, sizeof(pa_devicelist_t) * 16);

	// Create a mainloop API and connection to the default server
	pa_ml = pa_mainloop_new();
	pa_mlapi = pa_mainloop_get_api(pa_ml);
	pa_ctx = pa_context_new(pa_mlapi, "test");

	// This function connects to the pulse server
	pa_context_connect(pa_ctx, NULL, 0, NULL);


	// This function defines a callback so the server will tell us it's state.
	// Our callback will wait for the state to be ready.  The callback will
	// modify the variable to 1 so we know when we have a connection and it's
	// ready.
	// If there's an error, the callback will set pa_ready to 2
	pa_context_set_state_callback(pa_ctx, pa_state_cb, &pa_ready);

	// Now we'll enter into an infinite loop until we get the data we receive
	// or if there's an error

	for (;;) {
		// We can't do anything until PA is ready, so just iterate the mainloop
		// and continue
		if (pa_ready == 0) {
			pa_mainloop_iterate(pa_ml, 1, NULL);
			continue;
		}
		// We couldn't get a connection to the server, so exit out
		if (pa_ready == 2) {
			pa_context_disconnect(pa_ctx);
			pa_context_unref(pa_ctx);
			pa_mainloop_free(pa_ml);
			return -1;
		}
		// At this point, we're connected to the server and ready to make
		// requests

		switch (state) {
			// State 0: we haven't done anything yet
			case 0:
			// This sends an operation to the server.  pa_sinklist_info is
			// our callback function and a pointer to our devicelist will
			// be passed to the callback The operation ID is stored in the
			// pa_op variable
			pa_op = pa_context_get_sink_info_list(pa_ctx,
				pa_sinklist_cb,
				output
				);

			// Update state for next iteration through the loop
			state++;
			break;
			case 1:
			// Now we wait for our operation to complete.  When it's
			// complete our pa_output_devicelist is filled out, and we move
			// along to the next state
			if (pa_operation_get_state(pa_op) == PA_OPERATION_DONE) {
				pa_operation_unref(pa_op);

				// Now we perform another operation to get the source
				// (input device) list just like before.  This time we pass
				// a pointer to our input structure
				pa_op = pa_context_get_source_info_list(pa_ctx,
					pa_sourcelist_cb,
					input
					);
				// Update the state so we know what to do next
				state++;
			}
			break;
			case 2:
			if (pa_operation_get_state(pa_op) == PA_OPERATION_DONE) {
				// Now we're done, clean up and disconnect and return
				pa_operation_unref(pa_op);
				pa_context_disconnect(pa_ctx);
				pa_context_unref(pa_ctx);
				pa_mainloop_free(pa_ml);
				return 0;
			}
			break;
			default:
			// We should never see this state
			fprintf(stderr, "in state %d\n", state);
			return -1;
		}
		// Iterate the main loop and go again.  The second argument is whether
		// or not the iteration should block until something is ready to be
		// done.  Set it to zero for non-blocking.
		pa_mainloop_iterate(pa_ml, 1, NULL);
	}
}

/* A simple routine calling UNIX write() in a loop */
static ssize_t loop_write(int fd, const void*data, size_t size) {
	ssize_t ret = 0;
	while (size > 0) {
		ssize_t r;
		if ((r = write(fd, data, size)) < 0)
			return r;
		if (r == 0)
			break;
		ret += r;
		data = (const uint8_t*) data + r;
		size -= (size_t) r;
	}
	return ret;
}


int ctr;
// This is where we'll store the input device list
pa_devicelist_t pa_input_devicelist[16];

// This is where we'll store the output device list
pa_devicelist_t pa_output_devicelist[16];

char *InputSource = NULL;
char *OutputSource = NULL;


/* The sample type to use */
static const pa_sample_spec ss = {
	.format = PA_SAMPLE_S16LE,//PA_SAMPLE_S16BE,//PA_SAMPLE_S16LE,
	.rate = 48000,//48000,//44100,
	.channels = 1//2
};

pa_simple *s = NULL;
pa_simple *t = NULL;
int ret = 1;
int error;
//short buf[BUFFER_SIZE];


const char * SHARED_IQ_GetErrorString( int resultCode )
{
	switch( resultCode )
	{
	default:
	case SHARED_IQ_ERROR_UNKNOWN           : return "unknown error occurred";
	case SHARED_IQ_OK                      : return "no error";
	case SHARED_IQ_ERROR_PARAMS            : return "parameters are invalid";
	case SHARED_IQ_ERROR_OPEN              : return "could not open instance (allocation error?/tuner not available?)";
	case SHARED_IQ_ERROR_NO_DATA_AVAILABLE : return "no data available in internal buffers";
	}
}

typedef struct
{
	int          mode;
	unsigned int frequency;
	unsigned int sampleRate;
	short        buffer[8192 * 4];
	unsigned int bufCnt;
} Context;

int SHARED_IQ_CheckIfAvailable( unsigned int * sources )
{
	*sources = 0;
	printf("SHARED_IQ_CHECKIFAVAILABLE\n");

	if (pa_get_devicelist(pa_input_devicelist, pa_output_devicelist) < 0) {
		fprintf(stderr, "failed to get device list\n");
		//return 1;
	}

	for (ctr = 0; ctr < 16; ctr++){
		if (! pa_output_devicelist[ctr].initialized) {
			break;
		}
	}

	for (ctr = 0; ctr < 16; ctr++) {
		if (! pa_input_devicelist[ctr].initialized) {
			break;
		}
	}

	InputSource = pa_input_devicelist[1].name;

	/* enable as AM Source*/
	*sources |= 1 << SHARED_IQ_AM;

	/* enable as FM Source*/
	*sources |= 1 << SHARED_IQ_FM;

	/* enable as DAB Source*/
	//*sources |= 1 << SHARED_IQ_DAB;

	/* enable as DRM30 Source*/
	*sources |= 1 << SHARED_IQ_DRM30;

	/* enable as DRMPLUS Source*/
	*sources |= 1 << SHARED_IQ_DRMPLUS;

	return SHARED_IQ_OK;//SHARED_IQ_ERROR_NOT_AVAILABLE;
}

int SHARED_IQ_Open( SHARED_IQ_HANDLE * instance )
{
	Context * c = (Context*)malloc( sizeof( Context ) );

	/* initialize context */
	c->mode       = SHARED_IQ_OFF;
	c->frequency  = 0;
	c->sampleRate = 0;
	c->bufCnt     = 0;

	/* TODO: open tuner handle, start parallel thread to receive IQ samples into rounded buffer, etc */

	*instance = (SHARED_IQ_HANDLE)c;

	printf("SHARED_IQ_Open\n");

	if (!(s = pa_simple_new(NULL, NULL, PA_STREAM_RECORD, InputSource, "record", &ss, NULL, NULL, &error))) {
		fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
		return SHARED_IQ_ERROR_OPEN;
	}


//  filename = (char*) malloc(sizeof(char) * 1024);
//  if (filename == NULL) {
//    printf("Error in malloc\n");
//    exit(1);
//  }

//  // get file path
//  char cwd[1024];
//  if (getcwd(cwd, sizeof(cwd)) != NULL) {
   
// 	strcpy(filename, cwd);

// 	// get filename from command line
// 	if (argc < 2) {
// 	  printf("No wave file specified\n");
// 	  return;
// 	}
	
// 	strcat(filename, "/");
// 	strcat(filename, argv[1]);
// 	printf("%s\n", filename);
//  }

 // open file
 printf("Opening  file..\n");
 ptr = fopen(filename, "rb");
 if (ptr == NULL) {
	printf("Error opening file\n");
	exit(1);
 }
 

 mread = fread(header.riff, sizeof(header.riff), 1, ptr);
 printf("(1-4): %s \n", header.riff); 

 mread = fread(buffer4, sizeof(buffer4), 1, ptr);
 printf("%u %u %u %u\n", buffer4[0], buffer4[1], buffer4[2], buffer4[3]);
 
 // convert little endian to big endian 4 byte int
 header.overall_size  = buffer4[0] | 
						(buffer4[1]<<8) | 
						(buffer4[2]<<16) | 
						(buffer4[3]<<24);

 printf("(5-8) Overall size: bytes:%u, Kb:%u \n", header.overall_size, header.overall_size/1024);

 mread = fread(header.wave, sizeof(header.wave), 1, ptr);
 printf("(9-12) Wave marker: %s\n", header.wave);

 mread = fread(header.fmt_chunk_marker, sizeof(header.fmt_chunk_marker), 1, ptr);
 printf("(13-16) Fmt marker: %s\n", header.fmt_chunk_marker);

 mread = fread(buffer4, sizeof(buffer4), 1, ptr);
 printf("%u %u %u %u\n", buffer4[0], buffer4[1], buffer4[2], buffer4[3]);

 // convert little endian to big endian 4 byte integer
 header.length_of_fmt = buffer4[0] |
							(buffer4[1] << 8) |
							(buffer4[2] << 16) |
							(buffer4[3] << 24);
 printf("(17-20) Length of Fmt header: %u \n", header.length_of_fmt);

 mread = fread(buffer2, sizeof(buffer2), 1, ptr); printf("%u %u \n", buffer2[0], buffer2[1]);
 
 header.format_type = buffer2[0] | (buffer2[1] << 8);
 char format_name[10] = "";
 if (header.format_type == 1)
   strcpy(format_name,"PCM"); 
 else if (header.format_type == 6)
  strcpy(format_name, "A-law");
 else if (header.format_type == 7)
  strcpy(format_name, "Mu-law");

 printf("(21-22) Format type: %u %s \n", header.format_type, format_name);

 mread = fread(buffer2, sizeof(buffer2), 1, ptr);
 printf("%u %u \n", buffer2[0], buffer2[1]);

 header.channels = buffer2[0] | (buffer2[1] << 8);
 printf("(23-24) Channels: %u \n", header.channels);

 mread = fread(buffer4, sizeof(buffer4), 1, ptr);
 printf("%u %u %u %u\n", buffer4[0], buffer4[1], buffer4[2], buffer4[3]);

 header.sample_rate = buffer4[0] |
						(buffer4[1] << 8) |
						(buffer4[2] << 16) |
						(buffer4[3] << 24);

 printf("(25-28) Sample rate: %u\n", header.sample_rate);

 mread = fread(buffer4, sizeof(buffer4), 1, ptr);
 printf("%u %u %u %u\n", buffer4[0], buffer4[1], buffer4[2], buffer4[3]);

 header.byterate  = buffer4[0] |
						(buffer4[1] << 8) |
						(buffer4[2] << 16) |
						(buffer4[3] << 24);
 printf("(29-32) Byte Rate: %u , Bit Rate:%u\n", header.byterate, header.byterate*8);

 mread = fread(buffer2, sizeof(buffer2), 1, ptr);
 printf("%u %u \n", buffer2[0], buffer2[1]);

 header.block_align = buffer2[0] |
					(buffer2[1] << 8);
 printf("(33-34) Block Alignment: %u \n", header.block_align);

 mread = fread(buffer2, sizeof(buffer2), 1, ptr);
 printf("%u %u \n", buffer2[0], buffer2[1]);

 header.bits_per_sample = buffer2[0] |
					(buffer2[1] << 8);
 printf("(35-36) Bits per sample: %u \n", header.bits_per_sample);

 mread = fread(header.data_chunk_header, sizeof(header.data_chunk_header), 1, ptr);
 printf("(37-40) Data Marker: %s \n", header.data_chunk_header);

 mread = fread(buffer4, sizeof(buffer4), 1, ptr);
 printf("%u %u %u %u\n", buffer4[0], buffer4[1], buffer4[2], buffer4[3]);

 header.data_size = buffer4[0] |
				(buffer4[1] << 8) |
				(buffer4[2] << 16) | 
				(buffer4[3] << 24 );
 printf("(41-44) Size of data chunk: %u \n", header.data_size);
 


 // calculate no.of samples
  num_samples = (8 * header.data_size) / (header.channels * header.bits_per_sample);
 printf("Number of samples:%lu \n", num_samples);

  size_of_each_sample = (header.channels * header.bits_per_sample) / 8;
 printf("Size of each sample:%ld bytes\n", size_of_each_sample);

 // calculate duration of file
  duration_in_seconds = (float) header.overall_size / header.byterate;
 printf("Approx.Duration in seconds=%f\n", duration_in_seconds);
 printf("Approx.Duration in h:m:s=%s\n", seconds_to_time(duration_in_seconds));



 // read each sample from data chunk if PCM
 if (header.format_type == 1) { // PCM
 
		// make sure that the bytes-per-sample is completely divisible by num.of channels
		 bytes_in_each_channel = (size_of_each_sample / header.channels);
		if ((bytes_in_each_channel  * header.channels) != size_of_each_sample) {
			printf("Error: %ld x %ud <> %ld\n", bytes_in_each_channel, header.channels, size_of_each_sample);
			size_is_correct = FALSE;
		}
 
		if (size_is_correct) { 
					// the valid amplitude range for values based on the bits per sample

			switch (header.bits_per_sample) {
				case 8:
					low_limit = -128;
					high_limit = 127;
					break;
				case 16:
					low_limit = -32768;
					high_limit = 32767;
					break;
				case 32:
					low_limit = -2147483648;
					high_limit = 2147483647;
					break;
			}					

			printf("\n\n.Valid range for data values : %ld to %ld \n", low_limit, high_limit);

		}

 }else {
	printf("Error reading file. %d bytes\n", mread);
}


	c->sampleRate = ss.rate;

	return SHARED_IQ_OK;
}

int SHARED_IQ_Close( SHARED_IQ_HANDLE * instance )
{
	if( !instance || !*instance )
		return SHARED_IQ_ERROR_PARAMS;

	free( *instance );
	*instance = 0;

	if (s)
		pa_simple_free(s);

 	if (header.format_type == 1) { 
	printf("Closing file..\n");
	fclose(ptr);
	// cleanup before quitting
	free(filename);
	}


	return SHARED_IQ_OK;
}

int SHARED_IQ_Reset( SHARED_IQ_HANDLE instance )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	/* TODO: reset device etc */

	/* reset internal buffer */
	c->bufCnt = 0;
	return SHARED_IQ_OK;
}

int SHARED_IQ_GetConfig( SHARED_IQ_HANDLE instance, int * mode, unsigned int * frequency )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	*mode      = c->mode;
	*frequency = c->frequency;
	return SHARED_IQ_OK;
}

int SHARED_IQ_GetSampleRate( SHARED_IQ_HANDLE instance, unsigned int * sampleRate )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	/* use the following samplerate (in Hz): */
	/* SHARED_IQ_AM      =   24000 */
	/* SHARED_IQ_DRM30   =   24000 */
	/* SHARED_IQ_FM      =  192000 */
	/* SHARED_IQ_DRMPLUS =  192000 */
	/* SHARED_IQ_DAB     = 2048000 */

	*sampleRate = c->sampleRate;

#ifdef DO_DOWNSAMPLE_SHARED_IQ
	if( (c->mode == SHARED_IQ_DRM30 || c->mode == SHARED_IQ_AM ) && c->sampleRate == 48000 )
	{
		*sampleRate = c->sampleRate / 2;
	}
#endif
	return SHARED_IQ_OK;
}

int SHARED_IQ_Tune( SHARED_IQ_HANDLE instance, int mode, unsigned int frequency )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	/* for DRM30 there is an added offset of 4500 Hz */

	/* TODO: setup tuner */

	/* save settings */
	c->mode      = mode;
	c->frequency = frequency;
	return SHARED_IQ_OK;
}

int SHARED_IQ_AvailableData( SHARED_IQ_HANDLE instance, unsigned int * bytesAvailable )
{
	Context * c = (Context*)instance;
	if( !c || !bytesAvailable )
		return SHARED_IQ_ERROR_PARAMS;


	long i =0;
	if((num_samples - 8192*i_container) > 8192)
	{
		c->bufCnt = 8192;

	}else{
		c->bufCnt = (num_samples - 8192*i_container);
	}

	i_container++;

	// dump the data read
	unsigned int  xchannels = 0;
	int data_in_channel = 0;
	char data_buffer[size_of_each_sample];

	for (i =1; i <= c->bufCnt; i++) {
		//printf("==========Sample %ld / %ld=============\n", i, num_samples);
		mread = fread(data_buffer, sizeof(data_buffer), 1, ptr);
		// mread = fread(c->buffer, sizeof(short), c->bufCnt, ptr);
		if (mread == 1) {
		//printf("%c,",data_buffer);
			for (xchannels = 0; xchannels < header.channels; xchannels ++ ) {
				//printf("Channel#%d : ", (xchannels+1));
				// convert data from little endian to big endian based on bytes in each channel sample
				if (bytes_in_each_channel == 4) {
					data_in_channel =	data_buffer[0] | 
										(data_buffer[1]<<8) | 
										(data_buffer[2]<<16) | 
										(data_buffer[3]<<24);
				}
				else if (bytes_in_each_channel == 2) {
					data_in_channel = data_buffer[0] |
										(data_buffer[1] << 8);
				}
				else if (bytes_in_each_channel == 1) {
					data_in_channel = data_buffer[0];
				}

//				printf("%d, ", data_in_channel);
				c->buffer[i] = data_in_channel;

		// 		// check if value was in range
		// 		if (data_in_channel < low_limit || data_in_channel > high_limit)
		// 			printf("**value out of range\n");

		// 		//printf(" | ");
			}
		
		}
	}
	printf("\n");

	*bytesAvailable = c->bufCnt * sizeof( short );
	printf("SHARED_IQ_AvailableData = %d\n",c->bufCnt);
	return SHARED_IQ_OK;
}

int SHARED_IQ_GetData( SHARED_IQ_HANDLE instance, short * buffer, unsigned int bufferSize )
{
	Context * c = (Context*)instance;
	if( !c || !buffer || (bufferSize < 1) )
		return SHARED_IQ_ERROR_PARAMS;

	unsigned int bufferSizeNew = bufferSize;

#ifdef DO_DOWNSAMPLE_SHARED_IQ
	unsigned int i;
	if( (c->mode == SHARED_IQ_DRM30 || c->mode == SHARED_IQ_AM ) && ss.rate == 48000 )
	{
		bufferSizeNew = bufferSize * 2;
	}
#endif

	// if (pa_simple_read(s, c->buffer, bufferSizeNew, &error) < 0) {
	// 	fprintf(stderr, __FILE__": pa_simple_read() failed: %s\n", pa_strerror(error));
	// 	c->bufCnt = 0;
	// }else{
	// 	c->bufCnt = bufferSizeNew;
	// }

#ifdef DO_DOWNSAMPLE_SHARED_IQ
	if( (c->mode == SHARED_IQ_DRM30 || c->mode == SHARED_IQ_AM ) && ss.rate == 48000 )
	{
		// down sampling
		for( i = 0; i < c->bufCnt / 4; i++ )
		{
			c->buffer[i * 2 + 0] = c->buffer[i * 4 + 0] + c->buffer[i * 4 + 2];
			c->buffer[i * 2 + 1] = c->buffer[i * 4 + 1] + c->buffer[i * 4 + 3];
		}
		c->bufCnt /= 2;
	}
#endif

	/* copy contents from internal buffer */
	if( bufferSize > c->bufCnt)
	{
		bufferSize = c->bufCnt;
		printf("bufferSize = c->bufCnt");
	}

	if( bufferSize < c->bufCnt )
	{
		printf("return Buffer to small %d < %d\n", bufferSize, c->bufCnt);
	}

	//printf("SHARED_IQ_NewData  : %s\n",buf);
	(void)memcpy( buffer, c->buffer, bufferSize * sizeof(short));

	printf("SavedData  : %d %d\n",c->bufCnt,bufferSize);
	/* clear buffer */
	c->bufCnt = 0;
	printf("ReturningDataSize %d\n",bufferSize);
	return bufferSize;
}

