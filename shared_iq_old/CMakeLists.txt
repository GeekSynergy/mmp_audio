cmake_minimum_required( VERSION 2.8.7 )

set( PROJECT_TARGET shared_iq )

#include( ${CMAKE_CURRENT_LIST_DIR}/cmake/fhg_project.cmake )

project( ${PROJECT_TARGET} C )

set( CPACK_PACKAGE_VENDOR  "Fraunhofer" )
set( CPACK_PACKAGE_VERSION "1.0.0" )

################################################################################
# VARIABLES
################################################################################
if( NOT WIN32 AND NOT CMAKE_BUILD_TYPE )
  set( CMAKE_BUILD_TYPE "Release" CACHE STRING "" FORCE )
  message( STATUS "Build type not specified: defaulting to 'Release'." )
endif()
if( NOT DIST_PATH )
  set( DIST_PATH ${PROJECT_BINARY_DIR}/dist CACHE STRING "Destination of distribution files" )
endif()
set( EXECUTABLE_OUTPUT_PATH ${DIST_PATH}/bin )
set( LIBRARY_OUTPUT_PATH    ${DIST_PATH}/lib )

set_property( GLOBAL PROPERTY USE_FOLDERS ON )
set_property( GLOBAL APPEND_STRING PROPERTY PREDEFINED_TARGETS_FOLDER "cmake" )

################################################################################
# OPTIONS
################################################################################
if( NOT HAS_PARENT )
  option( ENABLE_SHARED_IQ_DEMO "Enable the Demo Application" OFF)
endif()

################################################################################
# DEFINITIONS
################################################################################
if( MSVC )
  add_definitions( /EHsc /D_CRT_SECURE_NO_DEPRECATE /D_CRT_NONSTDC_NO_DEPRECATE )
endif()
if( WIN32 )
  add_definitions( /DWIN32 )
endif()
if( UNIX )
  add_definitions( -D__POSIX__ -Wall -D__LINUX__ -DLINUX )
endif()
if( APPLE )
  add_definitions( -D__APPLE__ )
endif()
if( CMAKE_COMPILER_IS_GNUCC AND NOT WIN32 )
  # http://gcc.gnu.org/wiki/Visibility
  add_definitions( -Wall -Wextra -Wno-unused -Wsign-compare -fvisibility=hidden )
endif()

################################################################################
# PATHS
################################################################################
include_directories(
  ${PROJECT_SOURCE_DIR}
)

################################################################################
# SOURCES
################################################################################
set( FILES
  ${PROJECT_SOURCE_DIR}/shared_iq.h
  ${PROJECT_SOURCE_DIR}/shared_iq.c
)
source_group( main FILES ${FILES} )

################################################################################
# TARGETS
################################################################################
add_library( ${PROJECT_TARGET} SHARED
  ${FILES}

)

target_link_libraries( ${PROJECT_TARGET} -lpulse -lstdc++ -lpulse-simple)


message( STATUS "Pre ENABLE_SHARED_IQ_DEMO" )

if( ENABLE_SHARED_IQ_DEMO )
  add_executable( demo ${PROJECT_SOURCE_DIR}/demo.c )
  target_link_libraries( demo ${LIBS} ${PROJECT_TARGET} -lpulse -lstdc++ -lpulse-simple)
  message( STATUS "target_link_libraries( demo ${LIBS} ${PROJECT_TARGET} -lpulse -lstdc++ -lpulse-simple)" )
  # copy shared lib to binary path
  if( MSVC )
    add_custom_command(
      TARGET ${PROJECT_TARGET} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${DIST_PATH}/lib/$<$<CONFIG:Debug>:Debug>$<$<CONFIG:Release>:Release>/shared_iq.dll ${DIST_PATH}/bin/$<$<CONFIG:Debug>:Debug>$<$<CONFIG:Release>:Release>/
    )
  endif()
endif()

add_custom_command(
  TARGET ${PROJECT_TARGET} POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E make_directory ${DIST_PATH}/include/
  COMMAND ${CMAKE_COMMAND} -E copy_if_different ${PROJECT_SOURCE_DIR}/shared_iq.h ${INCLUDE_OUTPUT_PATH} ${DIST_PATH}/include/
)
