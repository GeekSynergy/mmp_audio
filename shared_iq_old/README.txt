How to build:
-------------
sudo apt-get cmake
mkdir build
cd build
cmake ..
make

How to release:
---------------
zip -r libshared_iq.zip dist
