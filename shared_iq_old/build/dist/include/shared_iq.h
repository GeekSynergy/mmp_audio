#ifndef __SHARED_IQ_H__
#define __SHARED_IQ_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _MSC_VER
# define SHARED_IQ_API __attribute__ ((visibility("default")))
#else
# ifdef SHARED_IQ_EXPORTS
#  define SHARED_IQ_API __declspec(dllexport)
# else
#  define SHARED_IQ_API __declspec(dllimport)
# endif
#endif

/** available result codes */
enum SHARED_IQ_RESULT
{
	SHARED_IQ_OK                       = 0,
	SHARED_IQ_ERROR_UNKNOWN            = -1,
	SHARED_IQ_ERROR_NOT_AVAILABLE      = -2,
	SHARED_IQ_ERROR_PARAMS             = -3,
	SHARED_IQ_ERROR_OPEN               = -4,
	SHARED_IQ_ERROR_NO_DATA_AVAILABLE  = -5
};

/** available tuner modes */
enum SHARED_IQ_MODES
{
	SHARED_IQ_OFF = 0,
	SHARED_IQ_AM,
	SHARED_IQ_FM,
	SHARED_IQ_DAB,
	SHARED_IQ_DRM30,
	SHARED_IQ_DRMPLUS
};

/** the handle for the SHARED_IQ library */
typedef void * SHARED_IQ_HANDLE;

/** return an ASCII string stating the error message */
SHARED_IQ_API const char * SHARED_IQ_GetErrorString( int resultCode );

/** returns whether the SHARED_IQ tuner is available and ready for use */
SHARED_IQ_API int SHARED_IQ_CheckIfAvailable();

/** open a new instance of this library */
SHARED_IQ_API int SHARED_IQ_Open( SHARED_IQ_HANDLE * instance );

/** close a previous instance of the library */
SHARED_IQ_API int SHARED_IQ_Close( SHARED_IQ_HANDLE * instance );

/** reset internal buffers (e.g. used after certain timeout when reception problems occur etc) */
SHARED_IQ_API int SHARED_IQ_Reset( SHARED_IQ_HANDLE instance );

/** retrieve current tuner settings */
SHARED_IQ_API int SHARED_IQ_GetConfig( SHARED_IQ_HANDLE instance, int * mode, unsigned int * frequency );

/** tune to a given frequency at a given mode */
SHARED_IQ_API int SHARED_IQ_Tune( SHARED_IQ_HANDLE instance, int mode, unsigned int frequency );

/** check how many bytes are currently available on internal buffer */
SHARED_IQ_API int SHARED_IQ_AvailableData( SHARED_IQ_HANDLE instance, unsigned int * bytesAvailable );

/** retrieve the data from internal buffer */
SHARED_IQ_API int SHARED_IQ_GetData( SHARED_IQ_HANDLE instance, void * buffer, unsigned int bufferSize );

#ifdef __cplusplus
}
#endif

#endif/*__SHARED_IQ_H__*/
