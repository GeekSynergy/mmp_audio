#include <stdio.h>
#include "shared_iq.h"

#define BUFFER_SIZE (8192*4)
//#define BUFSIZE 1024


#define LOG( x, ... ){ fprintf( stderr, x, ##__VA_ARGS__ ); }
#define CHECK_ERROR( fn )\
	if( err != SHARED_IQ_OK )\
	{\
		fprintf( stderr, fn "() -> error: '%s'\n", SHARED_IQ_GetErrorString( err ) );\
		return -1;\
	}

static const char * getModeString( int mode )
{
	switch( mode )
	{
	default:
	case SHARED_IQ_OFF     : return "OFF";
	case SHARED_IQ_AM      : return "AM";
	case SHARED_IQ_FM      : return "FM";
	case SHARED_IQ_DAB     : return "DAB";
	case SHARED_IQ_DRM30   : return "DRM30";
	case SHARED_IQ_DRMPLUS : return "DRM+";
	}
}



int main( int argc, char ** argv )
{
	
	SHARED_IQ_HANDLE shared_iq;
	int err, mode;
	unsigned int fq, bytesAvailable;
	unsigned char buf[BUFFER_SIZE];
	

	LOG( "SHARED_IQ demo application:\n" );
	
	/* check if tuner available */
	err = SHARED_IQ_CheckIfAvailable();
	LOG( "..tuner availability: %s\n", (err == SHARED_IQ_OK) ? "true" : "false" );
	
	/* open tuner handle */
	err = SHARED_IQ_Open( &shared_iq );
	CHECK_ERROR( "SHARED_IQ_Open" )
	LOG( "..library initialized\n" );
	
	/* print info on current settings */
	err = SHARED_IQ_GetConfig( shared_iq, &mode, &fq );
	CHECK_ERROR( "SHARED_IQ_GetConfig" )
	LOG( "..current settings: mode[%s] frequency[%d kHz]\n", getModeString( mode ), fq / 1000 );
	
	/* tune to German national Multiplex */
	err = SHARED_IQ_Tune( shared_iq, SHARED_IQ_DAB, 178352000 );
	CHECK_ERROR( "SHARED_IQ_Tune" )
	LOG( "..tuned to German multiplex\n" );
	
	/* read some samples */
	err = SHARED_IQ_AvailableData( shared_iq, &bytesAvailable );
	CHECK_ERROR( "SHARED_IQ_AvailableData" )
	LOG( "SHARED_IQ_AvailableData %d\n", bytesAvailable);
	if( bytesAvailable )
	{
		LOG( "Requested BUFFER_SIZE %d\n", BUFFER_SIZE);
		int datazixe = SHARED_IQ_GetData( shared_iq, &buf, BUFFER_SIZE );
		CHECK_ERROR( "SHARED_IQ_GetData" )
		LOG( "..successfully read %d bytes\n", datazixe );
	}
	
	/* clean up */
	err = SHARED_IQ_Close( &shared_iq );
	CHECK_ERROR( "SHARED_IQ_Close" )
	
	LOG( "done.\n" );
	return 0;
}
