#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#define SHARED_IQ_EXPORTS
#include "shared_iq.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <pulse/pulseaudio.h>

#define BUFFER_SIZE (8192)//(8192*4)
//#define BUFSIZE 1024


// Field list is here: http://0pointer.de/lennart/projects/pulseaudio/doxygen/structpa__sink__info.html
typedef struct pa_devicelist {
	uint8_t initialized;
	char name[512];
	uint32_t index;
	char description[256];
} pa_devicelist_t;

void pa_state_cb(pa_context *c, void *userdata);
void pa_sinklist_cb(pa_context *c, const pa_sink_info *l, int eol, void *userdata);
void pa_sourcelist_cb(pa_context *c, const pa_source_info *l, int eol, void *userdata);
int pa_get_devicelist(pa_devicelist_t *input, pa_devicelist_t *output);

//Audio Methods

// This callback gets called when our context changes state.  We really only
// care about when it's ready or if it has failed
void pa_state_cb(pa_context *c, void *userdata) {
	pa_context_state_t state;
	int *pa_ready = userdata;

	state = pa_context_get_state(c);
	switch  (state) {
		// There are just here for reference
		case PA_CONTEXT_UNCONNECTED:
		case PA_CONTEXT_CONNECTING:
		case PA_CONTEXT_AUTHORIZING:
		case PA_CONTEXT_SETTING_NAME:
		default:
			break;
		case PA_CONTEXT_FAILED:
		case PA_CONTEXT_TERMINATED:
			*pa_ready = 2;
			break;
		case PA_CONTEXT_READY:
			*pa_ready = 1;
			break;
	}
}

// pa_mainloop will call this function when it's ready to tell us about a sink.
// Since we're not threading, there's no need for mutexes on the devicelist
// structure
void pa_sinklist_cb(pa_context *c, const pa_sink_info *l, int eol, void *userdata) {
    pa_devicelist_t *pa_devicelist = userdata;
    int ctr = 0;

    // If eol is set to a positive number, you're at the end of the list
    if (eol > 0) {
	return;
    }

    // We know we've allocated 16 slots to hold devices.  Loop through our
    // structure and find the first one that's "uninitialized."  Copy the
    // contents into it and we're done.  If we receive more than 16 devices,
    // they're going to get dropped.  You could make this dynamically allocate
    // space for the device list, but this is a simple example.
    for (ctr = 0; ctr < 16; ctr++) {
	if (! pa_devicelist[ctr].initialized) {
	    strncpy(pa_devicelist[ctr].name, l->name, 511);
	    strncpy(pa_devicelist[ctr].description, l->description, 255);
	    pa_devicelist[ctr].index = l->index;
	    pa_devicelist[ctr].initialized = 1;
	    break;
	}
    }
}

// See above.  This callback is pretty much identical to the previous
void pa_sourcelist_cb(pa_context *c, const pa_source_info *l, int eol, void *userdata) {
    pa_devicelist_t *pa_devicelist = userdata;
    int ctr = 0;

    if (eol > 0) {
	return;
    }

    for (ctr = 0; ctr < 16; ctr++) {
	if (! pa_devicelist[ctr].initialized) {
	    strncpy(pa_devicelist[ctr].name, l->name, 511);
	    strncpy(pa_devicelist[ctr].description, l->description, 255);
	    pa_devicelist[ctr].index = l->index;
	    pa_devicelist[ctr].initialized = 1;
	    break;
	}
    }
}

int pa_get_devicelist(pa_devicelist_t *input, pa_devicelist_t *output) {
    // Define our pulse audio loop and connection variables
    pa_mainloop *pa_ml;
    pa_mainloop_api *pa_mlapi;
    pa_operation *pa_op;
    pa_context *pa_ctx;


    // We'll need these state variables to keep track of our requests
    int state = 0;
    int pa_ready = 0;

    // Initialize our device lists
    memset(input, 0, sizeof(pa_devicelist_t) * 16);
    memset(output, 0, sizeof(pa_devicelist_t) * 16);

    // Create a mainloop API and connection to the default server
    pa_ml = pa_mainloop_new();
    pa_mlapi = pa_mainloop_get_api(pa_ml);
    pa_ctx = pa_context_new(pa_mlapi, "test");

    // This function connects to the pulse server
    pa_context_connect(pa_ctx, NULL, 0, NULL);


    // This function defines a callback so the server will tell us it's state.
    // Our callback will wait for the state to be ready.  The callback will
    // modify the variable to 1 so we know when we have a connection and it's
    // ready.
    // If there's an error, the callback will set pa_ready to 2
    pa_context_set_state_callback(pa_ctx, pa_state_cb, &pa_ready);

    // Now we'll enter into an infinite loop until we get the data we receive
    // or if there's an error

    for (;;) {
	// We can't do anything until PA is ready, so just iterate the mainloop
	// and continue
	if (pa_ready == 0) {
		pa_mainloop_iterate(pa_ml, 1, NULL);
		continue;
	}
	// We couldn't get a connection to the server, so exit out
	if (pa_ready == 2) {
	    pa_context_disconnect(pa_ctx);
	    pa_context_unref(pa_ctx);
	    pa_mainloop_free(pa_ml);
	    return -1;
	}
	// At this point, we're connected to the server and ready to make
	// requests
	

	switch (state) {
	    // State 0: we haven't done anything yet
	    case 0:
		// This sends an operation to the server.  pa_sinklist_info is
		// our callback function and a pointer to our devicelist will
		// be passed to the callback The operation ID is stored in the
		// pa_op variable
		pa_op = pa_context_get_sink_info_list(pa_ctx,
			pa_sinklist_cb,
			output
			);

		// Update state for next iteration through the loop
		state++;
		break;
	    case 1:
		// Now we wait for our operation to complete.  When it's
		// complete our pa_output_devicelist is filled out, and we move
		// along to the next state
		if (pa_operation_get_state(pa_op) == PA_OPERATION_DONE) {
		    pa_operation_unref(pa_op);

		    // Now we perform another operation to get the source
		    // (input device) list just like before.  This time we pass
		    // a pointer to our input structure
		    pa_op = pa_context_get_source_info_list(pa_ctx,
			    pa_sourcelist_cb,
			    input
			    );
		    // Update the state so we know what to do next
		    state++;
		}
		break;
	    case 2:
		if (pa_operation_get_state(pa_op) == PA_OPERATION_DONE) {
		    // Now we're done, clean up and disconnect and return
		    pa_operation_unref(pa_op);
		    pa_context_disconnect(pa_ctx);
		    pa_context_unref(pa_ctx);
		    pa_mainloop_free(pa_ml);
		    return 0;
		}
		break;
	    default:
		// We should never see this state
		fprintf(stderr, "in state %d\n", state);
		return -1;
	}
	// Iterate the main loop and go again.  The second argument is whether
	// or not the iteration should block until something is ready to be
	// done.  Set it to zero for non-blocking.
	pa_mainloop_iterate(pa_ml, 1, NULL);
    }
}

/* A simple routine calling UNIX write() in a loop */
static ssize_t loop_write(int fd, const void*data, size_t size) {
    ssize_t ret = 0;
    while (size > 0) {
        ssize_t r;
        if ((r = write(fd, data, size)) < 0)
            return r;
        if (r == 0)
            break;
        ret += r;
        data = (const uint8_t*) data + r;
        size -= (size_t) r;
    }
    return ret;
}


int ctr;
// This is where we'll store the input device list
pa_devicelist_t pa_input_devicelist[16];

// This is where we'll store the output device list
pa_devicelist_t pa_output_devicelist[16];

char *InputSource = NULL;
char *OutputSource = NULL;


/* The sample type to use */
static const pa_sample_spec ss = {
	.format = PA_SAMPLE_S16LE,//PA_SAMPLE_S16BE,//PA_SAMPLE_S16LE,
	.rate = 44100,//48000,//44100,
	.channels = 1//2
};

pa_simple *s = NULL;
pa_simple *t = NULL;
int ret = 1;
int error;
//short buf[BUFFER_SIZE];


const char * SHARED_IQ_GetErrorString( int resultCode )
{
	switch( resultCode )
	{
	default:
	case SHARED_IQ_ERROR_UNKNOWN           : return "unknown error occurred";
	case SHARED_IQ_OK                      : return "no error";
	case SHARED_IQ_ERROR_PARAMS            : return "parameters are invalid";
	case SHARED_IQ_ERROR_OPEN              : return "could not open instance (allocation error?/tuner not available?)";
	case SHARED_IQ_ERROR_NO_DATA_AVAILABLE : return "no data available in internal buffers";
	}
}

typedef struct
{
	int           mode;
	unsigned int  frequency;
	short  buffer[BUFFER_SIZE];
	unsigned int  bufCnt;
} Context;

int SHARED_IQ_CheckIfAvailable()
{
	printf("SHARED_IQ_CHECKIFAVAILABLE\n");

	if (pa_get_devicelist(pa_input_devicelist, pa_output_devicelist) < 0) {
	  fprintf(stderr, "failed to get device list\n");
	  //return 1;
    	}

	for (ctr = 0; ctr < 16; ctr++){
	  if (! pa_output_devicelist[ctr].initialized) {
	    break;
	   }
    	}

    	for (ctr = 0; ctr < 16; ctr++) {
	  if (! pa_input_devicelist[ctr].initialized) {
	    break;
	 }
    	}

	InputSource = pa_input_devicelist[1].name;

	return SHARED_IQ_OK;//SHARED_IQ_ERROR_NOT_AVAILABLE;
}

int SHARED_IQ_Open( SHARED_IQ_HANDLE * instance )
{
	Context * c = (Context*)malloc( sizeof( Context ) );

	/* initialize context */
	c->mode      = SHARED_IQ_OFF;
	c->frequency = 0;
	c->bufCnt    = 0;

	/* TODO: open tuner handle, start parallel thread to receive IQ samples into rounded buffer, etc */

	*instance = (SHARED_IQ_HANDLE)c;

	printf("SHARED_IQ_Open\n");

	if (!(s = pa_simple_new(NULL, NULL, PA_STREAM_RECORD, InputSource, "record", &ss, NULL, NULL, &error))) {
		fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
		return SHARED_IQ_ERROR_OPEN;
	}
	else
	{
		return SHARED_IQ_OK;
	}
}

int SHARED_IQ_Close( SHARED_IQ_HANDLE * instance )
{
	if( !instance || !*instance )
		return SHARED_IQ_ERROR_PARAMS;

	free( *instance );
	*instance = 0;

	if (s)
    	pa_simple_free(s);

	return SHARED_IQ_OK;
}

int SHARED_IQ_Reset( SHARED_IQ_HANDLE instance )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	/* TODO: reset device etc */

	/* reset internal buffer */
	c->bufCnt = 0;
	return SHARED_IQ_OK;
}

int SHARED_IQ_GetConfig( SHARED_IQ_HANDLE instance, int * mode, unsigned int * frequency )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	*mode      = c->mode;
	*frequency = c->frequency;
	return SHARED_IQ_OK;
}

int SHARED_IQ_Tune( SHARED_IQ_HANDLE instance, int mode, unsigned int frequency )
{
	Context * c = (Context*)instance;
	if( !c )
		return SHARED_IQ_ERROR_PARAMS;

	/* TODO: setup tuner */

	/* save settings */
	c->mode      = mode;
	c->frequency = frequency;
	return SHARED_IQ_OK;
}

int SHARED_IQ_AvailableData( SHARED_IQ_HANDLE instance, unsigned int * bytesAvailable )
{
	Context * c = (Context*)instance;
	if( !c || !bytesAvailable )
		return SHARED_IQ_ERROR_PARAMS;


	/* TODO: implement synchronized round buffer etc */

	if (pa_simple_read(s, c->buffer, sizeof(c->buffer), &error) < 0) {
		fprintf(stderr, __FILE__": pa_simple_read() failed: %s\n", pa_strerror(error));
		// goto finish;
		c->bufCnt = 0;
	}else{
		c->bufCnt = sizeof(c->buffer);
	}


	*bytesAvailable = c->bufCnt;
	printf("SHARED_IQ_AvailableData = %d\n",c->bufCnt);
	return SHARED_IQ_OK;
}

int SHARED_IQ_GetData( SHARED_IQ_HANDLE instance, void * buffer, unsigned int bufferSize )
{
	Context * c = (Context*)instance;
	//if( !c || !buffer || (bufferSize < 1) )
	//	return SHARED_IQ_ERROR_PARAMS;

	//(void)memcpy( c->buffer ,buf, c->bufCnt );

	/* copy contents from internal buffer */
	//if( bufferSize > c->bufCnt)
	//	bufferSize = c->bufCnt;

	//printf("SHARED_IQ_NewData  : %s\n",buf);
	printf("SHARED_IQ_Size  : %d\n",bufferSize);

	(void)memcpy( buffer, c->buffer, bufferSize );

	//printf("SavedData  : %s\n",buffer);
	/* clear buffer */
	c->bufCnt = 0;
	return bufferSize;
}
